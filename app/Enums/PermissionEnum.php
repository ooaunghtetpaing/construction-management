<?php

namespace App\Enums;

enum PermissionEnum: string
{
    /** AUTH */
    case AUTH_UPDATE = 'AUTH_UPDATE';

    /** USER */
    case USER_INDEX = 'USER_INDEX';
    case USER_SHOW = 'USER_SHOW';
    case USER_STORE = 'USER_STORE';
    case USER_UPDATE = 'USER_UPDATE';
    case USER_DESTROY = 'USER_DESTROY';

    /** CLIENT*/
    case CLIENT_INDEX = 'CLIENT_INDEX';
    case CLIENT_SHOW = 'CLIENT_SHOW';
    case CLIENT_STORE = 'CLIENT_STORE';
    case CLIENT_UPDATE = 'CLIENT_UPDATE';
    case CLIENT_DESTROY = 'CLIENT_DESTROY';

    /** CONTRACTOR  */
    case CONTRACTOR_INDEX = 'CONTRACTOR_INDEX';
    case CONTRACTOR_SHOW = 'CONTRACTOR_SHOW';
    case CONTRACTOR_STORE = 'CONTRACTOR_STORE';
    case CONTRACTOR_UPDATE = 'CONTRACTOR_UPDATE';
    case CONTRACTOR_DESTROY = 'CONTRACTOR_DESTROY';

    /** EMPLOYEE_TYPE  */
    case EMPLOYEE_TYPE_INDEX = 'EMPLOYEE_TYPE_INDEX';
    case EMPLOYEE_TYPE_SHOW = 'EMPLOYEE_TYPE_SHOW';
    case EMPLOYEE_TYPE_STORE = 'EMPLOYEE_TYPE_STORE';
    case EMPLOYEE_TYPE_UPDATE = 'EMPLOYEE_TYPE_UPDATE';
    case EMPLOYEE_TYPE_DESTROY = 'EMPLOYEE_TYPE_DESTROY';

    /** EMPLOYEE */
    case EMPLOYEE_STORE = 'EMPLOYEE_STORE';
    case EMPLOYEE_SHOW = 'EMPLOYEE_SHOW';
    case EMPLOYEE_UPDATE = 'EMPLOYEE_UPDATE';
    case EMPLOYEE_INDEX = 'EMPLOYEE_INDEX';
    case EMPLOYEE_DESTROY = 'EMPLOYEE_DESTROY';
}
