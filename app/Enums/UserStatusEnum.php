<?php

namespace App\Enums;

enum UserStatusEnum: string
{
    case ACTIVE = 'ACTIVE';
    case DISABLE = 'DISABLE';
    case BLOCK = 'BLOCK';
    case VERIFIED = 'VERIFIED';
}
