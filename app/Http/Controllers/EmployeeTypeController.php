<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeTypeStoreRequest;
use App\Http\Requests\EmployeeTypeUpdateRequest;
use App\Models\EmployeeType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        DB::beginTransaction();
        try {
            $employeeType = EmployeeType::searchQuery()->sortingQuery()->paginationQuery();
            DB::commit();

            return $this->success('employeetype list are successfully retrived', $employeeType);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(EmployeeTypeStoreRequest $request)
    {

        $payload = collect($request->validated());
        DB::beginTransaction();
        try {
            $employeeType = EmployeeType::create($payload->toArray());
            DB::commit();

            return $this->success('New employee type is created successfully', $employeeType);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        DB::beginTransaction();
        try {
            $employeeType = EmployeeType::findOrFail($id);
            DB::commit();

            return $this->success('Employeetype detail is retrieved successfully', $employeeType);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(EmployeeTypeUpdateRequest $request, $id)
    {
        $payload = collect($request->validated());
        DB::beginTransaction();
        try {
            $employeeType = EmployeeType::findOrFail($id);
            $employeeType->update($payload->toArray());
            DB::commit();

            return $this->success('Employeetype is updated successfully', $employeeType);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $employeeType = EmployeeType::findOrFail($id);
            $employeeType->delete($id);
            DB::commit();

            return $this->success('Employeetype is deleted', $employeeType);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
