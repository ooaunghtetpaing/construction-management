<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContractorStoreRequest;
use App\Http\Requests\ContractorUpdateRequest;
use App\Models\Contractor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContractorController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        DB::beginTransaction();
        try {
            $contractor = Contractor::searchQuery()->sortingQuery()->paginationQuery();
            DB::commit();

            return $this->success('contractor list are successfully retrived', $contractor);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ContractorStoreRequest $request)
    {
        $payload = collect($request->validated());
        DB::beginTransaction();
        try {
            $contractor = Contractor::create($payload->toArray());
            DB::commit();

            return $this->success('New contractor is created successfully', $contractor);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {

        DB::beginTransaction();
        try {
            $contractor = Contractor::findOrFail($id);
            DB::commit();

            return $this->success('contractor detail is retrieved successfully', $contractor);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Contractor $contractor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ContractorUpdateRequest $request, $id)
    {
        $payload = collect($request->validated());

        DB::beginTransaction();
        try {
            $contractor = Contractor::findOrFail($id);
            $contractor->update($payload->toArray());
            DB::commit();

            return $this->success('contractor is updated successfully', $contractor);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {

        DB::beginTransaction();
        try {
            $contractor = Contractor::findOrFail($id);
            $contractor->delete($id);
            DB::commit();

            return $this->success('contractor is deleted', $contractor);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
