<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeStoreRequest;
use App\Http\Requests\EmployeeUpdateRequest;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @group Employee management
 *
 * @authenticated
 *
 * @header Authorization Bearer
 */
class EmployeeController extends Controller
{
    /**
     * APIs for retrive employee record
     *
     * @urlParam per_page integer. Example: 10
     * @urlParam page integer. Example: 1
     * @urlParam search string. Example: maungmaung
     * @urlParam columns string. Example: id,name,username,email,phone
     * @urlParam sort string. Example: id
     * @urlParam order string. Example: DESC
     */
    public function index(Request $request)
    {

        DB::beginTransaction();
        try {
            $employee = Employee::searchQuery()->sortingQuery()->paginationQuery();
            DB::commit();

            return $this->success('employee list are successfully retrived', $employee);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for create new employee record
     *
     * @bodyParam profile_picture.
     * @bodyParam father_name.
     * @bodyParam mother_name.
     * @bodyParam dob.
     * @bodyParam nrc.
     * @bodyParam position.
     * @bodyParam salary.
     * @bodyParam address.
     */
    public function store(EmployeeStoreRequest $request)
    {
        $payload = collect($request->validated());

        DB::beginTransaction();
        try {
            $employee = Employee::create($payload->toArray());
            DB::commit();

            return $this->success('New employee is created successfully', $employee);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for show employee record by id
     */
    public function show($id)
    {
        DB::beginTransaction();
        try {
            $employee = Employee::findOrFail($id);
            DB::commit();

            return $this->success('Employee detail is retrieved successfully', $employee);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for update employee record
     *
     * @bodyParam profile_picture.
     * @bodyParam father_name.
     * @bodyParam mother_name.
     * @bodyParam dob.
     * @bodyParam nrc.
     * @bodyParam position.
     * @bodyParam salary.
     * @bodyParam address.
     */
    public function update(EmployeeUpdateRequest $request, $id)
    {
        $payload = collect($request->validated());
        DB::beginTransaction();
        try {
            $employee = Employee::findOrFail($id);
            $employee->update($payload->toArray());
            DB::commit();

            return $this->success('Employee is updated successfully', $employee);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * APIs for delete employee record by id
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $employee = Employee::findOrFail($id);
            $employee->delete($id);
            DB::commit();

            return $this->success('Employee is deleted', $employee);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
