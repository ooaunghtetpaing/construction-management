<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClientStoreRequest;
use App\Http\Requests\ClientUpdateRequest;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        DB::beginTransaction();
        try {
            $client = Client::searchQuery()->sortingQuery()->paginationQuery();
            DB::commit();

            return $this->success('client list are successfully retrived', $client);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ClientStoreRequest $request)
    {
        $payload = collect($request->validated());
        DB::beginTransaction();
        try {
            $client = Client::create($payload->toArray());
            DB::commit();

            return $this->success('New client is created successfully', $client);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        DB::beginTransaction();
        try {
            $client = Client::findOrFail($id);
            DB::commit();

            return $this->success('client detail is retrieved successfully', $client);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ClientUpdateRequest $request, $id)
    {
        $payload = collect($request->validated());

        DB::beginTransaction();
        try {
            $client = Client::findOrFail($id);
            $client->update($payload->toArray());
            DB::commit();

            return $this->success('client is updated successfully', $client);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {

        DB::beginTransaction();
        try {
            $client = Client::findOrFail($id);
            $client->delete($id);
            DB::commit();

            return $this->success('client is deleted', $client);
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
