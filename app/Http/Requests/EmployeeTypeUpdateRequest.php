<?php

namespace App\Http\Requests;

use App\Models\EmployeeType;
use Illuminate\Foundation\Http\FormRequest;

class EmployeeTypeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $employeeType = EmployeeType::findOrFail(request()->id);
        $employeeTypeId = $employeeType->id;

        return [
            'name' => "unique:employee_types,name,$employeeTypeId",
            'description' => 'string',
        ];
    }
}
