<?php

namespace App\Http\Requests;

use App\Enums\REGXEnum;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $mobileRule = REGXEnum::MOBILE_NUMBER->value;
        $user = User::FindOrFail(request()->id);
        $userId = $user->id;

        return [
            'username' => ['string', 'max: 24', 'min: 6', "unique:users,username,$userId"],
            'full_name' => 'string', 'max: 24', 'min:6',
            'email' => "nullable|email|unique:users,email,$userId",
            'phone' => ['string', "unique:users,phone,$userId", "regex:$mobileRule"],
        ];
    }
}
