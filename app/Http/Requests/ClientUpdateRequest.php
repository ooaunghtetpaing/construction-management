<?php

namespace App\Http\Requests;

use App\Enums\REGXEnum;
use App\Models\Client;
use Illuminate\Foundation\Http\FormRequest;

class ClientUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $client = Client::findOrFail(request()->id);
        $clientId = $client->id;

        $mobileRule = REGXEnum::MOBILE_NUMBER->value;

        return [

            'name' => 'string',
            'phone' => ["unique:clients,phone,$clientId", 'string', "regex:$mobileRule"],
            'email' => "unique:clients,email,$clientId",
            'nrc' => 'nullable|string',

        ];
    }
}
