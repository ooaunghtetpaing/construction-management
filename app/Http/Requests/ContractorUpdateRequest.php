<?php

namespace App\Http\Requests;

use App\Enums\REGXEnum;
use App\Models\Contractor;
use Illuminate\Foundation\Http\FormRequest;

class ContractorUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $contractor = Contractor::findOrFail(request()->id);
        $contractorId = $contractor->id;

        $mobileRule = REGXEnum::MOBILE_NUMBER->value;

        return [

            'name' => 'string',
            'phone' => ["unique:contractors,phone,$contractorId", 'string', "regex:$mobileRule"],
            'email' => "unique:contractors,email,$contractorId",
            'nrc' => 'nullable|string',

        ];
    }
}
