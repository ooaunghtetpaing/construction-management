<?php

namespace App\Http\Requests;

use App\Models\Employee;
use App\Models\EmployeeType;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class EmployeeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {

        $employee = Employee::findOrFail(request()->id);
        $employeeId = $employee->id;

        $userIds = implode(',', User::all()->pluck('id')->toArray());
        $employee_typeIds = implode(',', EmployeeType::all()->pluck('id')->toArray());

        return [

            'user_id' => "in:$userIds|unique:employees,user_id",
            'employee_type_id' => "in:$employee_typeIds|unique:employees,employee_type_id",
            'nrc' => 'string | nullable',
            'father_name' => 'nullable|string',
            'mother_name' => 'nullable|string',
            'emergency_contact_person' => 'nullable|string',
            'emergency_contact_phone' => 'nullable|string|unique:employees,emergency_contact_phone',
            'phone' => 'nullable|string', 'unique:employees,phone',
            'email' => 'nullable|string|unique:employees,email',
            'join_on' => 'nullable|date',
            'leave_on' => 'nullable|date',
            'salary' => 'nullable',
            'job_description' => 'nullable',

        ];
    }
}
