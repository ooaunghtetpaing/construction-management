<?php

namespace App\Models;

use App\Traits\BasicAudit;
use App\Traits\HistoryRecord;
use App\Traits\SnowflakeID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasPermissions;
use Spatie\Permission\Traits\HasRoles;

class Employee extends Model
{
    use HasFactory,Notifiable, SnowflakeID, BasicAudit, SoftDeletes, HasPermissions, HasRoles, HistoryRecord;

    protected $fillable = [
        'employee_type_id', 'user_id', 'emergency_contact_person', 'emergency_contact_phone', 'phone', 'email', 'salary',
        'job_description', 'status',
    ];

    protected $casts = [
        'join_on' => 'date',
        'leave_on' => 'date',
    ];
}
