<?php

namespace App\Models;

use App\Traits\BasicAudit;
use App\Traits\HistoryRecord;
use App\Traits\SnowflakeID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasPermissions;
use Spatie\Permission\Traits\HasRoles;

class Client extends Model
{
    use HasFactory,Notifiable, SoftDeletes, HasPermissions, HasRoles, SnowflakeID, BasicAudit, HistoryRecord;

    protected $fillable = [
        'name', 'email', 'phone', 'nrc',
    ];
}
