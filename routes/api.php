<?php

use App\Enums\PermissionEnum;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ContractorController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\EmployeeTypeController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/refresh', [AuthController::class, 'refresh']);
    Route::post('/{id}/change-password', [AuthController::class, 'changePassword'])->permission(PermissionEnum::AUTH_UPDATE->value);
});

Route::middleware('jwt')->group(function () {

    Route::group(['prefix' => 'user'], function () {
        Route::post('/', [UserController::class, 'store'])->permission(PermissionEnum::USER_STORE->value);
        Route::get('/', [UserController::class, 'index'])->permission(PermissionEnum::USER_INDEX->value);
        Route::get('/{id}', [UserController::class, 'show'])->permission(PermissionEnum::USER_SHOW->value);
        Route::put('/{id}', [UserController::class, 'update'])->permission(PermissionEnum::USER_UPDATE->value);
        Route::delete('/{id}', [UserController::class, 'destroy'])->permission(PermissionEnum::USER_DESTROY->value);
    });

    Route::group(['prefix' => 'client'], function () {
        Route::post('/', [ClientController::class, 'store'])->permission(PermissionEnum::CLIENT_STORE->value);
        Route::get('/', [ClientController::class, 'index'])->permission(PermissionEnum::CLIENT_INDEX->value);
        Route::get('/{id}', [ClientController::class, 'show'])->permission(PermissionEnum::CLIENT_SHOW->value);
        Route::put('/{id}', [ClientController::class, 'update'])->permission(PermissionEnum::CLIENT_UPDATE->value);
        Route::delete('/{id}', [ClientController::class, 'destroy'])->permission(PermissionEnum::CLIENT_DESTROY->value);
    });

    Route::group(['prefix' => 'employee'], function () {

        Route::post('/', [EmployeeController::class, 'store'])->permission(PermissionEnum::EMPLOYEE_STORE->value);
        Route::get('/', [EmployeeController::class, 'index'])->permission(PermissionEnum::EMPLOYEE_INDEX->value);
        Route::get('/{id}', [EmployeeController::class, 'show'])->permission(PermissionEnum::EMPLOYEE_SHOW->value);
        Route::put('/{id}', [EmployeeController::class, 'update'])->permission(PermissionEnum::EMPLOYEE_UPDATE->value);
        Route::delete('/{id}', [EmployeeController::class, 'destroy'])->permission(PermissionEnum::EMPLOYEE_DESTROY->value);

        Route::group(['prefix' => 'type'], function () {
            Route::post('/', [EmployeeTypeController::class, 'store'])->permission(PermissionEnum::EMPLOYEE_TYPE_STORE->value);
            Route::get('/', [EmployeeTypeController::class, 'index'])->permission(PermissionEnum::EMPLOYEE_TYPE_INDEX->value);
            Route::get('/{id}', [EmployeeTypeController::class, 'show'])->permission(PermissionEnum::EMPLOYEE_TYPE_SHOW->value);
            Route::put('/{id}', [EmployeeTypeController::class, 'update'])->permission(PermissionEnum::EMPLOYEE_TYPE_UPDATE->value);
            Route::delete('/{id}', [EmployeeTypeController::class, 'destroy'])->permission(PermissionEnum::EMPLOYEE_TYPE_DESTROY->value);
        });
    });

    Route::group(['prefix' => 'contractor'], function () {
        Route::post('/', [ContractorController::class, 'store'])->permission(PermissionEnum::CONTRACTOR_STORE->value);
        Route::get('/', [ContractorController::class, 'index'])->permission(PermissionEnum::CONTRACTOR_INDEX->value);
        Route::get('/{id}', [ContractorController::class, 'show'])->permission(PermissionEnum::CONTRACTOR_SHOW->value);
        Route::put('/{id}', [ContractorController::class, 'update'])->permission(PermissionEnum::CONTRACTOR_UPDATE->value);
        Route::delete('/{id}', [ContractorController::class, 'destroy'])->permission(PermissionEnum::CONTRACTOR_DESTROY->value);
    });

});
