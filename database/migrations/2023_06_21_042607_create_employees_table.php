<?php

use App\Enums\GeneralStatusEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->snowflakeIdAndPrimary();
            $table->snowflakeId('employee_type_id');
            $table->snowflakeId('user_id');
            $table->string('emergency_contact_person')->nullable()->default(null);
            $table->string('emergency_contact_phone')->unique()->nullable()->default(null);
            $table->string('phone')->unique()->nullable();
            $table->string('email')->unique()->nullable()->default(null);
            $table->float('salary')->default(0);
            $table->date('join_on')->nullable()->default(null);
            $table->date('leave_on')->nullable()->default(null);
            $table->longText('job_description')->nullable()->default(null);
            $table->string('status')->default(GeneralStatusEnum::ACTIVE->value);
            $table->auditColumns();

            $table->foreign('employee_type_id')->references('id')->on('employee_types')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employees');
    }
};
