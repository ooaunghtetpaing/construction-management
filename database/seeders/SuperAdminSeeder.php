<?php

namespace Database\Seeders;

use App\Enums\GeneralStatusEnum;
use App\Enums\RoleEnum;
use App\Helpers\Enum;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $superAdmin = [
            'username' => 'superadmin',
            'name' => 'Super Admin',
            'profile' => null,
            'email' => 'superadmin@gpos.com',
            'phone' => '9421038123',
            'email_verified_at' => now(),
            'phone_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
            'status' => GeneralStatusEnum::ACTIVE->value,
        ];

        $roles = Enum::make(RoleEnum::class)->values();

        try {
            $user = User::updateOrCreate($superAdmin)->assignRole($roles);
        } catch (Exception $e) {
            throw new $e;
        }
    }
}
